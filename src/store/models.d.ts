export interface AuthenticationUser {
    token: string,
    permissions: string[],
    roles: string[]
}