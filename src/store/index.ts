import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

Vue.use(Vuex);

/** Modules */
import auth from "./modules/auth";
import countries from "./modules/countries";

const vuexPersistent = new VuexPersistence({
  modules: ['auth']
});

export default new Vuex.Store({
  modules: {
    auth: auth,
    countries: countries
  },
  plugins: [vuexPersistent.plugin]
})

Object.defineProperty(Vue.prototype, "$vuex", {
  get() {
    return this.$store;
  }
})