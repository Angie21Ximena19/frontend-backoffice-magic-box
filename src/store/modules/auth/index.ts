import Actions from "./actions";
import States from "./states";
import mutations from "./mutations";

export default {
    namespaced: true,
    actions: Actions,
    state: States,
    mutations: mutations
}