/* eslint-disable */
import Vue from "vue";
import axios from "axios";
import { User } from "./interfaces";
import store from "@/store";

const $this = Vue;

export default {
    authenticateUser(context: any, user: User): void {
        const loader = $this.$loading.show();
        axios.post('/auth/login', user)
        .then((res) => {
            loader.hide();
            if (res.data.transaction.status === true){
                context.commit('activeVerification');
            } else {
                $this.swal({
                    icon: "error",
                    text: res.data.message.content
                })
            }
        }).catch((err) => {
            loader.hide();
            console.log(err);
            $this.swal({
                icon: "error",
                text: "Ocurrio un problema al momento de validar tu identidad"
            });
        })
    },

    validateUser(context: any, user: User): void {
        const loader = $this.$loading.show();
        axios.post('/auth/validate-user', user)
        .then((res)  => {
            loader.hide();
            if (res.data.transaction.status === true){
                context.commit('validateUser', res.data.data);
                window.location.href = "/"
            } else {
                $this.swal({
                    icon: "error",
                    text: res.data.message.content
                })
            }
        }).catch((err) => {
            loader.hide();
            console.log(err);
            $this.swal({
                icon: "error",
                text: "Ocurrio un problema al momento de validar tu identidad."
            })
        })
    },

    getLoggedUser(context: any): void {
        const loader = $this.$loading.show();
        axios.get('/auth/get-logged-user')
        .then((res) => {
            loader.hide();
            if (res.data.transaction.status === true){
                context.commit('setLoggedUser', res.data.data);
            }
        }).catch((err) => {
            loader.hide();
            console.log(err);
            $this.swal({
                icon: "error",
                text: "Ocurrio un problema al momento de obtener al usuario logueado."
            })
        });
    }
}