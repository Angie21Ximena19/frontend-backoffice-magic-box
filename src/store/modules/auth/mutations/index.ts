import { AccessLogin, LoggedUser, State } from "./interfaces"

export default {
    activeVerification(state: State): void {
        state.verificationCode = true;
    },

    validateUser(state: State, data: AccessLogin): void {
        state.accessToken = data.token;
        state.permissions = data.permissions;
        state.roles = data.roles;
    },
    
    setLoggedUser(state: State, user: LoggedUser): void{
        state.loggedUser = user;
    },

    logout(state: State) : void {
        state.accessToken = "";
        state.roles = [];
        state.permissions = [];
        state.loggedUser = {};
        state.verificationCode = false;
        window.location.href = "/login"
    }
}