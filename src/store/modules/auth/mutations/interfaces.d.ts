export interface AccessLogin {
    token: string,
    roles: string[],
    permissions: string[],
}

export interface Pivot {
    model_id: number,
    model_type: string,
    role_id: number
}

export interface Role {
    created_at: string,
    guard_name: string,
    id: number,
    name: string,
    pivot: Pivot,
    updated_at: string
}

export interface LoggedUser {
    account_type_id: number,
    avatar: string | null,
    city_id: number,
    contraseña: string,
    created_at: string,
    date_first_order: string,
    date_last_order: string,
    document: string,
    document_type_id: number,
    email: string,
    fullname: string,
    gender: string,
    id: number,
    orders_number: 0,
    roles: Role[],
    status_id: number,
    uid: string,
    updated_at: string,
    verificationCode: string
}

export interface State {
    verificationCode: boolean,
    accessToken: string,
    roles: string[],
    permissions: string[],
    loggedUser: LoggedUser | object
}
