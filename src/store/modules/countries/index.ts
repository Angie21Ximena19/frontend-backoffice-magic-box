import actions from "./actions";
import mutations from "./mutations";
import states from "./states";

export default {
    namespaced: true,
    actions: actions,
    mutations: mutations,
    state: states
}