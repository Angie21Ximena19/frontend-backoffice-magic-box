export interface Status {
    id: number,
    name: string,
    color_status: string,
    translation_status: string,
    model: string,
    created_at: string,
    updated_at: string
}

export interface Countries {
    id: number,
    name: string,
    acronym: string,
    flag?: string,
    minimum_pay_by_cashback: string,
    maximum_pay_by_cashback: string,
    created_at: string,
    updated_at: string,
    status: Status
}

export interface State {
    countries?: Countries[] | Array
}