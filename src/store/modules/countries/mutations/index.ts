import { Countries, State } from "./interface";

export default {
    setCountries(state: State, data: Countries) : void {
        state.countries = data;
    }
}