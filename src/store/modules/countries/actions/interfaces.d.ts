export interface Data {
    ascending: number,
    byColumn: number,
    limit: number,
    page: number,
    query: string
}

export interface ChangeStatus {
    id: number,
    verb: string,
    action: string
}