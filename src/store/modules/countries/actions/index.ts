import axios from "axios";
import Vue from "vue";
import {ChangeStatus, Data} from "./interfaces";

export default {
  getCountries(context?: any): void {
    const loader = Vue.$loading.show();
    axios.post('/countries/get-countries')
      .then((res) => {
        loader.hide();
        context.commit('setCountries', res.data.data);
      }).catch((err) => {
      loader.hide();
      console.log(err);
      Vue.swal({
        icon: "error",
        text: "Ocurrio un problema al momento de obtener los paises"
      })
    })
  },

  changeStatus(context: any, data: ChangeStatus): void {
    const {
      id,
      verb,
      action
    } = data;

    Vue.swal({
      icon: "question",
      text: `Seguro que quieres ${verb} este país?`,
      cancelButtonText: 'Cancelar',
      cancelButtonColor: "#d50000",
      showCancelButton: true,
      confirmButtonText: `Si, ${action}`,
      confirmButtonColor: "#00c853"
    }).then((res) => {
      if (res.isConfirmed) {
        const loader = Vue.$loading.show();
        axios.post('/countries/change-status', {id})
          .then((response) => {
            loader.hide();
            if (response.data.transaction.status === true) {
              Vue.swal({
                icon: "success",
                text: "Se cambio el estado del país."
              })
              context.dispatch('getCountries');
            } else {
              Vue.swal({
                icon: 'error',
                text: "Ocurrio un problema al momento de cambiar el estado al país."
              })
            }
          }).catch((err) => {
          console.log(err);
          loader.hide();
          Vue.swal({
            icon: "error",
            text: "Ocurrio un problema al momento de cambiar el estado al país."
          })
        })
      }
    })
  },

  filterCountries(context: any, search: string | null): void {
    axios.post('/countries/get-countries', {search})
      .then((res) => {
        context.commit('setCountries', res.data.data);
      }).catch((err) => {
      console.log(err);
      Vue.swal({
        icon: "error",
        text: "Ocurrio un problema al momento de obtener los paises"
      })
    })
  }
}
