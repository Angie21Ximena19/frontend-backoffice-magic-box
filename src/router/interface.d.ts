export interface auth {
    accessToken: string,
    roles: string[],
    permissions: string[],
    verificationCode: boolean
}

export interface State {
    auth?: auth | undefined
}