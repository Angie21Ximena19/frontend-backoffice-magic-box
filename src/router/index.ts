/** eslint-disable */
import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue';
import store from '@/store'
import { State } from './interface';

Vue.use(VueRouter)

const stateStore: State = store.state

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Auth/Login.vue'),
    meta: {
      layout: "blank",
      title: "Login - Materio"
    },
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: "/countries",
    name: "Countries",
    component: () => import("@/views/Configuration/Countries/Countries.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name !== "Login" && stateStore.auth.accessToken === ""){
    next("/login");
  } else if (to.name === "Login" && stateStore.auth.accessToken !== ""){
    next('/');
  } else {
    next();
  }
});

export default router;