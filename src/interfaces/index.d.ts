export interface auth {
    accessToken: string,
    roles: string[],
    permissions: string[],
    verificationCode: boolean,
    loggedUser: object
}

export interface State {
    auth?: auth
}