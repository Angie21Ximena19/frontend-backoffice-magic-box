export interface Data {
    ascending: number,
    byColumn: number,
    limit: number,
    page: number,
    query: string
}

export interface Columns {
    text: string,
    value: string,
    sortable: boolean,
    align: string
}