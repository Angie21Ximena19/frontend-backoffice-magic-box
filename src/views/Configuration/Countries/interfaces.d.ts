export interface Country {
  name: string,
  acronym: string,
  maximum_pay_by_cashback: number,
  minimum_pay_by_cashback: number

}
