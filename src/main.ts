/**
 * Libraries
 */
import Vue from 'vue'
import VueCompositionAPI from "@vue/composition-api";
import Loading from "vue-loading-overlay";
import VueSweetalert2 from 'vue-sweetalert2';
import axios from 'axios';

/**
 * Components
 */
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

/** Styles */
import "vue-loading-overlay/dist/vue-loading.css";
import 'sweetalert2/dist/sweetalert2.min.css';
import { State } from './interfaces';

const storeState: State = store.state;

/** Axios */
axios.defaults.baseURL = "http://127.0.0.1:8000/api";
axios.defaults.headers = {
  Authorization: `Bearer ${storeState.auth.accessToken}`
}

/**
 * Vue Use
 */
Vue.use(VueCompositionAPI);
Vue.use(VueSweetalert2);
Vue.use(Loading, {
  color: "#9155fd"
});

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
