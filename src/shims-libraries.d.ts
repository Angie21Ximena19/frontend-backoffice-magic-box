import Vue from "vue";
import VueSweetalert2 from "vue-sweetalert2";
import Vuex from "vuex";

declare module "vue/types/vue" {
    interface Vue {
        readonly $swal: VueSweetalert2,
        readonly $store: Vuex
    }
}